import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Employee } from "./employee";

@Injectable({
  providedIn: "root",
})
export class EmployeeService {
  private baseUrl = "http://localhost:8080";
  constructor(private httpclient: HttpClient) {}

  getEmployeeList(): Observable<Employee[]> {
    return this.httpclient.get<Employee[]>(this.baseUrl);
  }

  createEmployee(employee: Employee): Observable<Object> {
    return this.httpclient.post(this.baseUrl, employee);
  }
}
