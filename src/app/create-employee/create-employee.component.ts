import {
  Component,
  EventEmitter,
  Input,
  NgModule,
  OnInit,
  Output,
} from "@angular/core";
import { Employee } from "../employee";
import { EmployeeListComponent } from "../employee-list/employee-list.component";
import { FormsModule } from "@angular/forms";
import { EmployeeService } from "../employee.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-create-employee",
  templateUrl: "./create-employee.component.html",
  styleUrls: ["./create-employee.component.scss"],
})
export class CreateEmployeeComponent implements OnInit {
  employee: Employee = new Employee();
  constructor(
    private employeeService: EmployeeService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  saveEmployee() {
    this.employeeService.createEmployee(this.employee).subscribe((data) => {
      console.log(data, "data");
    });
  }
  goToEmployeeList() {
    this.router.navigate(["/employees"]);
  }
  onSubmit() {
    this.saveEmployee();
    console.log(this.employee, "data za employee");
  }
}
