export class Employee {
  id?: number;
  firstName?: String;
  middleName?: String;
  lastName?: String;
  emailId?: String;
}
